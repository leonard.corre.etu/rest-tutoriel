package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.dto.InfoTauxDto2;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    
    
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
            return TauxTva.NORMAL.taux;
    }
    
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
      try {
          return TauxTva.valueOf(niveau.toUpperCase()).taux; 
      }
      catch ( Exception ex ) {
          throw new NiveauTvaInexistantException("Niveau de TVA inexistant");
      }

    }
    
    @GET
    @Path("{niveauTva}")
    public double getMontantTotal(@PathParam("niveauTva") String niveau,@QueryParam("somme") String prixht) {
        try {
            return calculTva.calculerMontant(TauxTva.valueOf(niveau.toUpperCase()), Integer.parseInt(prixht));
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException("La somme ou le Niveau de TVA n'est pas valide");
        }

    }
    
    
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }
    
    @GET
    @Path("details/{taux}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public InfoTauxDto2 getDetail(@PathParam("taux") String taux,@QueryParam("somme") String valeur) {
      InfoTauxDto2 res = new InfoTauxDto2(); 
      res.setMontantTotal(calculTva.calculerMontant(TauxTva.valueOf(taux.toUpperCase()), Integer.parseInt(valeur)));
      res.setMontantTva(TauxTva.valueOf(taux.toUpperCase()).taux);
      res.setSomme(Integer.parseInt(valeur));
      res.setTauxLabel(taux.toUpperCase());
      res.setTauxValue((TauxTva.valueOf(taux.toUpperCase()).taux));
      return res;
    }
    
    




}
